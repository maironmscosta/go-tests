package binarySearch

func search(list []int, number int, qtIteractions int) (bool, int) {

	qtIteractions++
	listLength := len(list)
	if listLength == 0 || (listLength == 1 && list[0] != number) {
		return false, qtIteractions
	}

	middleListPosition := listLength / 2
	numberOfTheMiddle := list[middleListPosition]

	if number == numberOfTheMiddle {
		return true, qtIteractions
	}

	if number > numberOfTheMiddle {
		return search(list[middleListPosition:], number, qtIteractions)
	}

	if number < numberOfTheMiddle {
		return search(list[:middleListPosition], number, qtIteractions)
	}

	return false, qtIteractions
}

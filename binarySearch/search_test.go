package binarySearch

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestBinarySearch(t *testing.T) {

	tests := []struct {
		Name           string
		NumberToFind   int
		TotalNumber    int
		ExpectedResult bool
	}{
		{
			Name:           "numa lista com numeros de 1 a 100, encontrar numero 9",
			NumberToFind:   9,
			TotalNumber:    100,
			ExpectedResult: true,
		},
		{
			Name:           "numa lista com numeros de 1 a 10, encontrar numero 11",
			NumberToFind:   11,
			TotalNumber:    10,
			ExpectedResult: false,
		},
		{
			Name:           "numa lista com numeros de 1 a 1000, encontrar numero 751",
			NumberToFind:   751,
			TotalNumber:    1000,
			ExpectedResult: true,
		},
	}

	for _, test := range tests {

		t.Run(test.Name, func(t *testing.T) {

			count := 1
			var list = make([]int, 0)
			for range test.TotalNumber {
				list = append(list, count)
				count++
			}

			exists, qtInteractions := search(list, test.NumberToFind, 0)
			exit := fmt.Sprintf("exists %v? %v - qtInteractions: %v", test.NumberToFind, exists, qtInteractions)
			fmt.Println(exit)
			assert.Equal(t, test.ExpectedResult, exists)

		})
	}

}

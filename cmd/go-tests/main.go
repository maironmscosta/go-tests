package main

import (
	"fmt"
)

func main() {

	fmt.Println(fmt.Sprintf("fffoiii Test %s", "GO"))

	messages := make(chan string, 2)
	defer close(messages)
	messages <- "buffered"
	fmt.Println("buffered")
	messages <- "channel"
	fmt.Println("channel")
	messages <- "test"
	fmt.Println("test")

	fmt.Println(<-messages)
	fmt.Println(<-messages)
	fmt.Println(<-messages)

}

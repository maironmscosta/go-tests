package fatorial

import "fmt"

type Factorial struct {
	Number int
}

//Desafio pra vocês bem fácil
//
//Entregar um programa para mim que faz o fatorial de um numero imprimindo
//na tela corretamente e depois seu resultado da soma. Utilize a
//linguagem de sua escolha.
//
//O que é fatorial? E como devo imprimir?: Fatorial falando de uma
//forma mais rapida seria: Fatorial de 5: 5x4x3x2x1 = 120. Deve ser
//imprimido na tela deste mesmo modo.
func (factorial *Factorial) run () (int, string) {

	result := factorial.Number
	var calc = fmt.Sprintf("%d", factorial.Number)

	for minNumber := 1; factorial.Number > minNumber; {
		factorial.Number--
		calc = addCalc(calc, factorial.Number)
		result = result * factorial.Number
	}

	return result, fmt.Sprintf("%s = %d", calc, result)
}

func addCalc (calc string, number int) string {

	return fmt.Sprintf("%sx%d", calc, number)
}

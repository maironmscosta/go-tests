package fatorial

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_Factorial(t *testing.T) {

	tests := []struct {
		Name         string
		Number       int
		ExpectResult int
		ExpectCalc   string
	}{
		{
			Name:         "Factorial de 1",
			Number:       1,
			ExpectResult: 1,
			ExpectCalc:   "1 = 1",
		}, {
			Name:         "Factorial de 2",
			Number:       2,
			ExpectResult: 2,
			ExpectCalc:   "2x1 = 2",
		}, {
			Name:         "Factorial de 5",
			Number:       5,
			ExpectResult: 120,
			ExpectCalc:   "5x4x3x2x1 = 120",
		}, {
			Name:         "Factorial de 7",
			Number:       7,
			ExpectResult: 5040,
			ExpectCalc:   "7x6x5x4x3x2x1 = 5040",
		}, {
			Name:         "Factorial de 10",
			Number:       10,
			ExpectResult: 3628800,
			ExpectCalc:   "10x9x8x7x6x5x4x3x2x1 = 3628800",
		},
	}

	for _, test := range tests {

		t.Run(test.Name, func(t *testing.T) {

			factorial := Factorial{
				Number: test.Number,
			}

			result, calc := factorial.run()
			assert.Equal(t, test.ExpectResult, result)
			assert.Equal(t, test.ExpectCalc, calc)

		})

	}

}

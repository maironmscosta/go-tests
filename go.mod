module bitbucket.org/maironmscosta/go-tests

go 1.22

require (
	github.com/jmoiron/jsonq v0.0.0-20150511023944-e874b168d07e
	github.com/lestrrat-go/jwx/v2 v2.0.0-beta2
	github.com/rabbitmq/amqp091-go v1.9.0
	github.com/skip2/go-qrcode v0.0.0-20200617195104-da1b6568686e
	github.com/stretchr/testify v1.8.0
	github.com/thedevsaddam/gojsonq/v2 v2.5.2
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/decred/dcrd/dcrec/secp256k1/v4 v4.0.1 // indirect
	github.com/goccy/go-json v0.9.6 // indirect
	github.com/lestrrat-go/blackmagic v1.0.1 // indirect
	github.com/lestrrat-go/httpcc v1.0.1 // indirect
	github.com/lestrrat-go/httprc v1.0.1 // indirect
	github.com/lestrrat-go/iter v1.0.2 // indirect
	github.com/lestrrat-go/option v1.0.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

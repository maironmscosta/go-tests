package hashGenerator

import (
	"fmt"
	"math/rand"
	"strings"
	"time"
)

type HashGenerator struct {
	HashForNumber map[int][]string
}

func NewHashGenerator() HashGenerator {
	return HashGenerator{
		HashForNumber: hashForNumber,
	}
}

var hashForNumber = map[int][]string{
	0:  {"a", "k", "u"},
	1:  {"b", "l", "v"},
	2:  {"c", "m", "w"},
	3:  {"d", "n", "x"},
	4:  {"e", "o", "y"},
	5:  {"f", "p", "z"},
	6:  {"g", "q", "_"},
	7:  {"h", "r", "-"},
	8:  {"i", "s", "0"},
	9:  {"j", "t", "!"},
	10: {"k"},
	11: {"l"},
	12: {"m"},
	13: {"n"},
	14: {"o"},
	15: {"p"},
	16: {"q"},
	17: {"r"},
	18: {"s"},
	19: {"t"},
	20: {"u"},
	21: {"v"},
	22: {"w"},
	23: {"x"},
	24: {"y"},
	25: {"z"},
	26: {"_"},
	27: {"-"},
	28: {"0"},
	29: {"1"},
	30: {"2"},
	31: {"3"},
}

func (hg HashGenerator) GetCharFromHash() string {

	//vec := strings.Split(fmt.Sprintf("%d", time.Now().UnixMilli()), "")
	//randNum := rand.Intn(12)
	//n, _ := strconv.Atoi(vec[randNum])

	return hg.GetCharFromNumber(rand.Intn(10))
	//return hg.GetCharFromNumber(9)
}

func (hg HashGenerator) GetCharFromNumber(n int) string {
	letterPosition := 0
	if n < 10 {
		letterPosition = rand.Intn(3)
	}

	letter := hg.HashForNumber[n][letterPosition]
	if rand.Intn(4)%2 == 0 {
		letter = strings.ToUpper(letter)
	}

	return letter
}

func (hg HashGenerator) HashGenerator(hashSize int) string {

	day := time.Now().Day()
	hour := time.Now().Hour()
	hash := fmt.Sprintf("%s%s", hg.GetCharFromNumber(day),
		hg.GetCharFromNumber(hour))

	for i := 1; i <= hashSize; i++ {
		hash += hg.GetCharFromHash()
	}

	return hash
}

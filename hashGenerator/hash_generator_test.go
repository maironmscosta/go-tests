package hashGenerator

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
	"time"
)

func TestHashGenerator(t *testing.T) {

	tests := []struct {
		Name string
		Size int
	}{
		{
			Name: "inform a test",
			Size: 5,
		},
	}

	for _, test := range tests {

		t.Run(test.Name, func(t *testing.T) {

			now := time.Now()
			var hashes = make(map[string]bool)
			var even = make(map[string]int)
			hashGenerator := NewHashGenerator()
			for i := 0; i < 3819816; i++ {
				hash := hashGenerator.HashGenerator(test.Size)
				exists := hashes[hash]
				if exists {
					even[hash] = even[hash] + 1
					continue
				}
				hashes[hash] = true
			}

			fmt.Println(fmt.Sprintf("repetidos: %d", len(even)))
			for k, v := range even {
				if v >= 2 {
					fmt.Println(fmt.Sprintf("hash: %s ; qtd: %d", k, v))
				}
			}

			fmt.Println(fmt.Sprintf("FIM. repetidos: %d Tempo Execucao: %v", len(even), time.Now().Sub(now).Seconds()))

		})

	}

}

func TestGetCharFromNumber(t *testing.T) {

	tests := []struct {
		Name            string
		Number          int
		ExpectedResults []string
	}{
		{
			Name:            "number 0",
			Number:          0,
			ExpectedResults: []string{"a", "k", "u"},
		},
		{
			Name:            "number 25",
			Number:          25,
			ExpectedResults: []string{"z"},
		},
	}

	for _, test := range tests {

		t.Run(test.Name, func(t *testing.T) {

			hashGenerator := NewHashGenerator()
			result := hashGenerator.GetCharFromNumber(test.Number)
			fmt.Println(fmt.Sprintf("letter: %s", result))

			exists := false
			for _, expected := range test.ExpectedResults {
				if strings.ToUpper(expected) == strings.ToUpper(result) {
					exists = true
				}
			}

			assert.True(t, true, exists)

		})

	}

}

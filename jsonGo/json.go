package jsonGo

import (
	"github.com/jmoiron/jsonq"
	gojsonq "github.com/thedevsaddam/gojsonq/v2"
)
func JsonQ (jsonMapper interface{}, key string) (interface{}, error){

	jsonQuery := jsonq.NewQuery(jsonMapper)
	val, err := jsonQuery.Interface(key)
	if err != nil {
		return nil, err
	}
	return val, nil
}

func GoJsonQ (goJsonQ, key string) interface{} {

	name := gojsonq.New().FromString(goJsonQ).Find(key)

	return name
}

package jsonGo

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestJsonQ(t *testing.T) {

	tests := []struct {
		Name       string
		JsonMapper map[string]interface{}
		Keys       map[string]interface{}
	}{
		{
			Name: "test jsonq",
			JsonMapper: map[string]interface{}{
				"valor01": 1,
				"valor02": "teste",
			},
			Keys: map[string]interface{}{
				"valor01": 1,
				"valor02": "teste",
			},
		},
	}

	for _, test := range tests {

		t.Run(test.Name, func(t *testing.T) {

			for key, val := range test.Keys {

				jsonValue, err := JsonQ(test.JsonMapper, key)
				if err != nil {
					panic(err)
				}
				assert.Equal(t, val, jsonValue)
			}
		})
	}
}

func TestGoJsonQ(t *testing.T) {

	tests := []struct {
		Name    string
		JsonStr string
		Keys    map[string]interface{}
	}{
		{
			Name:    "test GoJsonQ",
			JsonStr: `{"name":{"first":"Tom","last":"Hanks"},"age":61}`,
			Keys: map[string]interface{}{
				"name.first": "Tom",
				"age": float64(61),
			},
		},
		{
			Name:    "key not exists",
			JsonStr: `{"name": "Tom","age":61}`,
			Keys: map[string]interface{}{
				"name.first": nil,
				"age": float64(61),
			},
		},
	}

	for _, test := range tests {

		t.Run(test.Name, func(t *testing.T) {

			for key, val := range test.Keys {
				jsonValue := GoJsonQ(test.JsonStr, key)
				assert.Equal(t, val, jsonValue)
			}
		})
	}
}

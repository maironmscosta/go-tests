package jwt

import (
	"fmt"
	"github.com/lestrrat-go/jwx/v2/jwa"
	"github.com/lestrrat-go/jwx/v2/jwe"
	"log"
)

func GenerateJWEHS256(payload []byte, secret string) ([]byte, error) {

	enc, err := jwe.Encrypt(payload, jwe.WithKey(jwa.PBES2_HS256_A128KW, []byte(secret)), jwe.WithContentEncryption(jwa.A128CBC_HS256))
	if err != nil {
		fmt.Printf("failed to sign payload: %s\n", err)
		return enc, err
	}

	return enc, nil
}

func VerifiedJWEHS256(value []byte, secret string) ([]byte, error) {

	// When you receive a JWS message, you can verify the signature
	// and grab the payload sent in the message in one go:
	jweKey := jwe.WithKey(jwa.PBES2_HS256_A128KW, []byte(secret))
	decr, err := jwe.Decrypt(value, jweKey)
	if err != nil {
		log.Printf("failed to verify message: %s", err)
		return decr, nil
	}

	return decr, nil
}

package jwt

import (
	"encoding/json"
	"log"
	"testing"
)

func TestJWE_HS256(t *testing.T) {

	tests := []struct {
		Name  string
		Value map[string]string
	}{
		{
			Name: "jws test",
			Value: map[string]string{
				"nome": "Mairon",
				"ano":  "2022",
			},
		},
	}

	for _, test := range tests {

		t.Run(test.Name, func(t *testing.T) {

			hs := HMACSHA256GenerateKey()
			marshal, _ := json.Marshal(test.Value)
			jweEncr, err := GenerateJWEHS256(marshal, hs)
			if err != nil {
				log.Printf("failed to generate jwsSigned: %s", err)
				return
			}
			jweDecr, err := VerifiedJWEHS256(jweEncr, hs)
			if err != nil {
				log.Printf("failed to generate verifiedJWS: %s", err)
				return
			}

			log.Printf("signed message verified! -> %s", jweDecr)

		})
	}
}

package jwt

import (
	"crypto/rsa"
	"fmt"
	"github.com/lestrrat-go/jwx/v2/jwa"
	"github.com/lestrrat-go/jwx/v2/jws"
	"log"
)

func GenerateJWSRS256(payload []byte, privKey *rsa.PrivateKey) ([]byte, error) {

	signed, err := jws.Sign(payload, jws.WithKey(jwa.RS256, privKey))
	if err != nil {
		fmt.Printf("failed to sign payload: %s\n", err)
		return signed, err
	}

	return signed, nil
}

func VerifiedJWSRS256(value []byte, privKey *rsa.PrivateKey) ([]byte, error) {

	// When you receive a JWS message, you can verify the signature
	// and grab the payload sent in the message in one go:
	pubKey := &privKey.PublicKey
	verified, err := jws.Verify(value, jws.WithKey(jwa.RS256, pubKey))
	if err != nil {
		log.Printf("failed to verify message: %s", err)
		return verified, nil
	}

	return verified, nil
}

func GenerateJWSHS256(payload []byte, secret string) ([]byte, error) {

	signed, err := jws.Sign(payload, jws.WithKey(jwa.HS256, []byte(secret)))
	if err != nil {
		fmt.Printf("failed to sign payload: %s\n", err)
		return signed, err
	}

	return signed, nil
}

func VerifiedJWSHS256(value []byte, secret string) ([]byte, error) {

	// When you receive a JWS message, you can verify the signature
	// and grab the payload sent in the message in one go:
	verified, err := jws.Verify(value, jws.WithKey(jwa.HS256, []byte(secret)))
	if err != nil {
		log.Printf("failed to verify message: %s", err)
		return verified, nil
	}

	return verified, nil
}

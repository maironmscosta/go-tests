package jwt

import (
	"encoding/json"
	"log"
	"testing"
)

func TestJWS_RS256(t *testing.T) {

	tests := []struct {
		Name  string
		Value map[string]string
	}{
		{
			Name: "jws test",
			Value: map[string]string{
				"nome": "Mairon",
				"ano":  "2022",
			},
		},
	}

	for _, test := range tests {

		t.Run(test.Name, func(t *testing.T) {

			rsaKey, _ := RSAGenerateKey()
			marshal, _ := json.Marshal(test.Value)
			jwsSigned, err := GenerateJWSRS256(marshal, rsaKey)
			if err != nil {
				log.Printf("failed to generate jwsSigned: %s", err)
				return
			}
			verifiedJWS, err := VerifiedJWSRS256(jwsSigned, rsaKey)
			if err != nil {
				log.Printf("failed to generate verifiedJWS: %s", err)
				return
			}

			log.Printf("signed message verified! -> %s", verifiedJWS)

		})
	}
}

func TestJWS_HS256(t *testing.T) {

	tests := []struct {
		Name  string
		Value map[string]interface{}
	}{
		{
			Name: "jws test",
			Value: map[string]interface{}{
				"jti": "123ID",
				"sub": "test",
				"result": map[string]string{
					"nome": "Mairon",
					"ano":  "2022",
				},
			},
		},
	}

	for _, test := range tests {

		t.Run(test.Name, func(t *testing.T) {

			hs := HMACSHA256GenerateKey()
			marshal, _ := json.Marshal(test.Value)
			jwsSigned, err := GenerateJWSHS256(marshal, hs)
			if err != nil {
				log.Printf("failed to generate jwsSigned: %s", err)
				return
			}
			verifiedJWS, err := VerifiedJWSHS256(jwsSigned, hs)
			if err != nil {
				log.Printf("failed to generate verifiedJWS: %s", err)
				return
			}

			log.Printf("signed message verified! -> %s", verifiedJWS)

		})
	}
}

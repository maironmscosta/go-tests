package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

// https://github.com/guto-alves/loterias-api
func LoteriaAPI() {

	response, err := http.Get("https://loteriascaixa-api.herokuapp.com/api/mega-sena/latest")
	if err != nil {
		panic(err)
	}

	respBody, err := ioutil.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}

	fmt.Println(fmt.Sprintf("%v", string(respBody)))
}

func main() {
	LoteriaAPI()
}

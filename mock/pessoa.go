package mock

import "fmt"

type PessoaServiceInterface interface {
	Adicionar(pessoa Pessoa) bool
}

type Pessoa struct {
	Nome string
}

type PessoaService struct {}

func (p *PessoaService) Adicionar(pessoa Pessoa) bool {
	fmt.Sprintf("PessoaService - %s: %s", "adicionar pessoa com nome", pessoa.Nome)
	if len(pessoa.Nome) == 0 {
		return false
	}
	return true
}

type PessoaServiceMock struct {
	AdicionarMock func(pessoa Pessoa) bool
}

func (mock *PessoaServiceMock) Adicionar(pessoa Pessoa) bool {
	if mock.AdicionarMock != nil {
		return mock.AdicionarMock(pessoa)
	}
	return false
}
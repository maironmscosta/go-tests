package mock

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestPessoaService_Adicionar(t *testing.T) {

	tests := []struct {
		Nome          string
		Pessoa        Pessoa
		PessoaService PessoaServiceInterface
		ExpectReturn  bool
	}{
		{
			Nome: "PessoaServiceMock: adicionar pessoa",
			PessoaService: &PessoaServiceMock{
				AdicionarMock: func(pessoa Pessoa) bool {
					fmt.Sprintf("PessoaServiceMock - %s: %s", "adicionar pessoa com nome", pessoa.Nome)
					if len(pessoa.Nome) == 0 {
						return false
					}
					return true
				},
			},
			Pessoa: Pessoa{
				Nome: "Marilda",
			},
			ExpectReturn: true,
		},
		{
			Nome: "PessoaService: adicionar pessoa",
			PessoaService: &PessoaService{},
			Pessoa: Pessoa{},
			ExpectReturn: false,
		},
	}

	for _, test := range tests {
		t.Run(test.Nome, func(t *testing.T) {

			adicionado := test.PessoaService.Adicionar(test.Pessoa)
			assert.Equal(t, test.ExpectReturn, adicionado)

		})
	}

}

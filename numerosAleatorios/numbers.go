package main

import (
	"fmt"
	"math/rand"
	"sort"
)

func GerarNumerosAleatorios(quantidade, maiorDezenas, menorDezena int) (numbers []int) {

	numbersMap := make(map[int]int, 0)
	for i := 0; i < quantidade; i++ {
		number := rand.Intn(maiorDezenas + 1)
		_, exists := numbersMap[number]
		if number < menorDezena || exists {
			i--
			continue
		}
		numbersMap[number] = number
		numbers = append(numbers, number)
	}
	sort.Ints(numbers)
	return
}

func main() {
	for i := 0; i < 4; i++ {
		aleatorios := GerarNumerosAleatorios(6, 60, 1)
		fmt.Println(fmt.Sprintf("saida[%d]: %v", i+1, aleatorios))
	}

}

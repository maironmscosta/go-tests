package main

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGerarNumerosAleatorios(t *testing.T) {

	tests := []struct {
		Name              string
		QuantidadeDezenas int
		MaiorDezena       int
		MenorDezena       int
	}{
		{
			Name:              "gerar 6 dezenas de 60 numeros",
			QuantidadeDezenas: 6,
			MaiorDezena:       60,
			MenorDezena:       1,
		},
		{
			Name:              "gerar 15 dezenas de 25 numeros",
			QuantidadeDezenas: 15,
			MaiorDezena:       25,
			MenorDezena:       1,
		},
	}

	for _, test := range tests {

		t.Run(test.Name, func(t *testing.T) {

			numerosAleatorios := GerarNumerosAleatorios(test.QuantidadeDezenas, test.MaiorDezena, test.MenorDezena)

			fmt.Println(fmt.Sprintf("numerosAleatorios: %v", numerosAleatorios))
			assert.Equal(t, len(numerosAleatorios), test.QuantidadeDezenas)
			for _, v := range numerosAleatorios {
				assert.LessOrEqual(t, v, test.MaiorDezena)
				assert.GreaterOrEqual(t, v, test.MenorDezena)
			}

		})
	}

}

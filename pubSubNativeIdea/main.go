package main

import "C"
import (
	"fmt"
	"sync"
	"time"
)

var mu sync.Mutex
var i = 0

func main() {

	pubSubService, err := NewService()
	if err != nil {
		panic(err)
	}

	for _, v := range topics {
		createTopic, err := pubSubService.CreateTopic(v)
		if err != nil {
			panic(err)
		}
		fmt.Println(createTopic)
	}

	_, _ = pubSubService.Subscriber("http://test-not-multiple", topics["1"], topics["2"], topics["3"])
	_, _ = pubSubService.Subscriber("http://test-multiple-7", topics["2"])
	_, _ = pubSubService.Subscriber("http://test-multiple-7-11", topics["2"])
	_, _ = pubSubService.Subscriber("http://test-multiple-4", topics["3"])

	ticker := time.Tick(time.Second * 3)
	for range ticker {
		go func() {
			for i <= 50 {
				topic := getNameTopic(i)
				pubSubService.Publisher(topic, fmt.Sprintf("message [%d] to the topic [%s]", i, topic))
				//time.Sleep(time.Second * 1)
				incrementI()
				if i%15 == 0 {
					fmt.Println("######################################")
					break
				}
			}
		}()

	}

	time.Sleep(time.Second * 100)
}

func incrementI() {
	mu.Lock()
	i++
	mu.Unlock()
}

func getNameTopic(i int) string {
	if i%7 == 0 {
		return topics["2"]
	} else if i%4 == 0 {
		return topics["3"]
	}
	return "not-multiple"
}

var topics = map[string]string{
	"1": "not-multiple",
	"2": "multiple-7",
	"3": "multiple-4",
}

package main

import (
	"errors"
	"fmt"
	"time"
)

const (
	TopicDoesntExist         = "there is no topic [%s]"
	MessageTopicEmpty        = "topic [%s] or message [%s] are empties"
	NotificationAddressEmpty = "notification address [%s] is invalid"
	SubscribeSuccess         = "subscribe success to topic [%s]"
	PublishSuccess           = "publish success to topic [%s] message [%s]"
	CreateTopicSuccess       = "create topic [%s] success"
	CreateTopicExists        = "create topic [%s] already exists"
)

type Service struct {
	PubSub map[string]*PubSub
	Send   chan Send
}

type PubSub struct {
	Topic       string
	Subscribers []string
}

type Send struct {
	Message     string
	Subscribers []string
}

func NewService() (*Service, error) {

	pubSubs := make(map[string]*PubSub)
	sends := make(chan Send, 10)
	service := Service{
		PubSub: pubSubs,
		Send:   sends,
	}

	// notification
	go service.notification()

	return &service, nil
}

func (service *Service) CreateTopic(topic string) (string, error) {

	err := service.exists(topic)
	if err == nil {
		msg := fmt.Sprintf(CreateTopicExists, topic)
		fmt.Println(msg)
		return "", errors.New(msg)
	}

	service.PubSub[topic] = &PubSub{
		Topic: topic,
	}

	msg := fmt.Sprintf(CreateTopicSuccess, topic)
	fmt.Println(msg)
	return msg, nil
}

func (service *Service) Publisher(topic string, message string) (string, error) {

	if len(topic) == 0 || len(message) == 0 {
		msg := fmt.Sprintf(MessageTopicEmpty, topic, message)
		fmt.Println(msg)
		return "", errors.New(msg)
	}

	pubSub := service.PubSub[topic]
	if pubSub == nil {
		pubSub = &PubSub{
			Topic: topic,
		}
		service.PubSub[topic] = pubSub
	}

	service.Send <- Send{
		Message:     message,
		Subscribers: pubSub.Subscribers,
	}

	msg := fmt.Sprintf(PublishSuccess, topic, message)
	fmt.Println(msg)
	return msg, nil
}

func (service *Service) Subscriber(notificationAddress string, topics ...string) (string, error) {

	go func() error {
		for _, topic := range topics {

			err := service.exists(topic)
			if err != nil {
				return err
			}

			if len(notificationAddress) == 0 {
				msg := fmt.Sprintf(NotificationAddressEmpty, notificationAddress)
				fmt.Println(msg)
				return errors.New(msg)
			}

			pubSub := service.PubSub[topic]
			pubSub.Subscribers = append(pubSub.Subscribers, notificationAddress)

			msg := fmt.Sprintf(SubscribeSuccess, topic)
			fmt.Println(msg)
		}
		return nil
	}()

	return fmt.Sprintf(SubscribeSuccess, topics), nil
}

func (service *Service) exists(topic string) error {

	pubSub := service.PubSub[topic]
	if pubSub == nil {
		msg := fmt.Sprintf(TopicDoesntExist, topic)
		fmt.Println(msg)
		return errors.New(msg)
	}

	return nil
}

func (service *Service) notification() {
	for {
		time.Sleep(time.Second * 1)

		send := <-service.Send
		go func() {
			for _, address := range send.Subscribers {
				fmt.Println(fmt.Sprintf("send to address [%s] the message [%s]", address, send.Message))
			}
		}()
	}
}

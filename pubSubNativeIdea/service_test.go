package main

import (
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestService_Subscriber(t *testing.T) {

	tests := []struct {
		Name                   string
		Topic                  string
		NotificationAddress    string
		ShouldCreateEmptyTopic bool
		ExpectedResult         string
		ExpectedError          error
	}{
		{
			Name:           "should return error - topic test not exists",
			Topic:          "test",
			ExpectedError:  errors.New(fmt.Sprintf(TopicDoesntExist, "test")),
			ExpectedResult: "",
		},
		{
			Name:           "should return error - topic is empty",
			ExpectedError:  errors.New(fmt.Sprintf(TopicDoesntExist, "")),
			ExpectedResult: "",
		},
		{
			Name:                   "should return error - address notification is empty",
			Topic:                  "test",
			ShouldCreateEmptyTopic: true,
			ExpectedResult:         "",
			ExpectedError:          errors.New(fmt.Sprintf(NotificationAddressEmpty, "")),
		},
		{
			Name:                   "should return success",
			Topic:                  "test",
			NotificationAddress:    "notification_test",
			ShouldCreateEmptyTopic: true,
			ExpectedResult:         fmt.Sprintf(SubscribeSuccess, []string{"test"}),
		},
	}

	for _, test := range tests {

		t.Run(test.Name, func(t *testing.T) {

			service, _ := NewService()

			if test.ShouldCreateEmptyTopic {
				service.CreateTopic(test.Topic)
			}

			result, err := service.Subscriber(test.NotificationAddress, test.Topic)
			if err != nil {
				assert.Equal(t, test.ExpectedError, err)
			}

			assert.Equal(t, test.ExpectedResult, result)

		})

	}

}

func TestService_Publisher(t *testing.T) {

	tests := []struct {
		Name                   string
		Topic                  string
		Message                string
		ShouldCreateEmptyTopic bool
		ExpectedResult         string
		ExpectedError          error
	}{
		{
			Name:          "should return error - topic is empty",
			ExpectedError: errors.New(fmt.Sprintf(MessageTopicEmpty, "", "")),
		},
		{
			Name:          "should return error - message is empty",
			Topic:         "test",
			ExpectedError: errors.New(fmt.Sprintf(MessageTopicEmpty, "test", "")),
		},
		{
			Name:                   "should return success - message published into topic",
			Topic:                  "test",
			ShouldCreateEmptyTopic: true,
			Message:                "message published",
			ExpectedResult:         fmt.Sprintf(PublishSuccess, "test", "message published"),
		},
		{
			Name:           "should return success - topic test not exists but it was created",
			Topic:          "test",
			Message:        "message to publish",
			ExpectedError:  errors.New(fmt.Sprintf(TopicDoesntExist, "test")),
			ExpectedResult: fmt.Sprintf(PublishSuccess, "test", "message to publish"),
		},
	}

	for _, test := range tests {

		t.Run(test.Name, func(t *testing.T) {

			service, _ := NewService()
			if test.ShouldCreateEmptyTopic {
				service.CreateTopic(test.Topic)
			}

			result, err := service.Publisher(test.Topic, test.Message)
			if err != nil {
				assert.Equal(t, test.ExpectedError, err)
			}

			assert.Equal(t, test.ExpectedResult, result)
		})

	}
}

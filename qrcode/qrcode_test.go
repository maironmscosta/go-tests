package qrcode

import (
	"errors"
	"fmt"
	"github.com/skip2/go-qrcode"
	"github.com/stretchr/testify/assert"
	"image/color"
	"os"
	"strings"
	"testing"
)

func TestCreate256x256PNGImage(t *testing.T) {

	tests := []struct {
		Name string
		QRCode
		ExpectedResultImage bool
		ExpectedError       error
	}{
		{
			Name:                "error on create a 256x256 PNG image - no data to encode",
			QRCode:              QRCode{},
			ExpectedResultImage: false,
			ExpectedError:       errors.New("no data to encode"),
		},
		{
			Name: "success to create a 256x256 PNG image",
			QRCode: QRCode{
				RecoveryLevel: qrcode.Medium,
				ImageSize:     256,
				URL:           "https://google.com",
			},
			ExpectedResultImage: true,
			ExpectedError:       nil,
		},
	}

	for _, test := range tests {

		t.Run(test.Name, func(t *testing.T) {

			image, err := test.QRCode.Create256x256PNGImage()
			assert.Equal(t, test.ExpectedResultImage, image != nil)
			assert.Equal(t, test.ExpectedError, err)

		})

	}

}

func TestCreate256x256PNGImageAndWriteToFile(t *testing.T) {

	tests := []struct {
		Name string
		QRCode
		ExpectedError error
	}{
		{
			Name: "success to create a 256x256 PNG image",
			QRCode: QRCode{
				RecoveryLevel: qrcode.Medium,
				ImageSize:     256,
				URL:           "https://google.com",
				FileName:      "qrcode.png",
			},
			ExpectedError: nil,
		},
	}

	for _, test := range tests {

		t.Run(test.Name, func(t *testing.T) {

			err := test.QRCode.Create256x256PNGImageAndWriteToFile()
			assert.Equal(t, test.ExpectedError, err)

		})

	}

}

func TestCreate256x256PNGImageWithCustomColorsAndWriteToFile(t *testing.T) {

	tests := []struct {
		Name string
		QRCode
		ExpectedError error
	}{
		{
			Name: "success to create a 256x256 PNG image - background black and foreground white",
			QRCode: QRCode{
				RecoveryLevel:   qrcode.Medium,
				ImageSize:       256,
				URL:             "https://diolinux.com.br",
				FileName:        "diolinux.png",
				BackgroundColor: color.Black,
				ForegroundColor: color.White,
			},
			ExpectedError: nil,
		},
		{
			Name: "success to create a 256x256 PNG image - background black and foreground white",
			QRCode: QRCode{
				RecoveryLevel:   qrcode.Medium,
				ImageSize:       256,
				URL:             "https://github.com/skip2/go-qrcode",
				FileName:        "github_skip2.png",
				BackgroundColor: color.Black,
				ForegroundColor: Red,
			},
			ExpectedError: nil,
		},
	}

	for _, test := range tests {

		t.Run(test.Name, func(t *testing.T) {

			deleteFileTests()
			err := test.QRCode.Create256x256PNGImageWithCustomColorsAndWriteToFile()
			assert.Equal(t, test.ExpectedError, err)

		})

	}

}

func deleteFileTests() {
	dir, err := os.ReadDir(".")
	if err != nil {
		fmt.Println(fmt.Sprintf("error: %s", err))
		return
	}
	for _, entry := range dir {
		if strings.Contains(entry.Name(), ".png") {
			fmt.Println(entry.Name())
			os.RemoveAll(entry.Name())
		}
	}
}

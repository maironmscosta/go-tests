package main

import (
	"fmt"
	amqp "github.com/rabbitmq/amqp091-go"
	"log"
)

func openConnection() *amqp.Channel {
	urlConnection := fmt.Sprintf("amqp://%s:%s@%s:%s", "test", "test1234", "localhost", "5672")
	conn, err := amqp.Dial(urlConnection)
	if err != nil {
		//logger.Error("error to creating rabbitmq connection", "error", err.Error())
		panic(err)
	}

	ch, err := conn.Channel()
	if err != nil {
		//logger.Error("error to get channel from rabbitmq connection", "error", err.Error())
		panic(err)
	}
	return ch
}

func main() {

	channel := openConnection()

	q, err := channel.QueueDeclare(
		"account_create", // name
		false,            // durable
		false,            // delete when unused
		false,            // exclusive
		false,            // no-wait
		nil,              // arguments
	)

	if err != nil {
		panic(err)
	}
	var forever chan struct{}
	messages, err := channel.Consume(
		q.Name,       // queue
		"test-hex-1", // consumer
		false,        // auto-ack
		false,        // exclusive
		false,        // no-local
		false,        // no-wait
		nil,          // args
	)

	go func() {
		for message := range messages {
			fmt.Println(fmt.Sprintf("message: %v", string(message.Body)))
			message.Ack(true)
		}
	}()

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}

package thread

import (
	"fmt"
	"time"
)

type Channel struct {
}

//https://medium.com/trainingcenter/goroutines-e-go-channels-f019784d6855
func (c Channel) runUnbuffered(s string, done chan string) {
	time.Sleep(100 * time.Millisecond)
	done <- fmt.Sprintf("Terminei (%s)", s)
}

func (c Channel) runBuffered(vet []int, done chan string) {
	//defer close(done)
	for i, n := range vet {
		time.Sleep(100 * time.Millisecond)
		//fmt.Println(fmt.Sprintf("n: %d", n))
		sum := i + n
		done <- fmt.Sprintf("n: %d - Terminei (%d)", n, sum)
	}
	return
}

func (c Channel) runListenChannel(done string) {
	fmt.Println(fmt.Sprintf("done: %v", done))
}

func (c Channel) runBufferedQTD(qtd int, done chan string) {
	//defer close(done)
	//fmt.Println(fmt.Sprintf("qtd: %d", qtd))
	time.Sleep(100 * time.Millisecond)
	done <- fmt.Sprintf("Terminei (%d)", qtd)
	done <- fmt.Sprintf("Terminei (%d)", qtd * (-1))
	//done <- fmt.Sprintf("Terminei (%d)", qtd * 100)
}
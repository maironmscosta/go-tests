package thread

import (
	"fmt"
	"testing"
	"time"
)

func Test_Channel_Unbuffered(t *testing.T) {

	tests := []struct {
		Name         string
		Word         string
		ExpectResult int
		ExpectCalc   string
	}{
		{
			Word: "teste01",
		},
		{
			Word: "teste02",
		},
		{
			Word: "teste03",
		},
		{
			Word: "teste04",
		},
	}

	done := make(chan string)
	channel := Channel{}
	for _, test := range tests {

		t.Run(test.Name, func(t *testing.T) {

			go channel.runUnbuffered(test.Word, done)
			fmt.Println("Processando...")

			fmt.Println(<-done)
		})

	}

}


func Test_Channel_Buffered(t *testing.T) {

	//https://gobyexample.com/channel-buffering
	t.Run("teste channel", func(t *testing.T) {

		done := make(chan string, 2)
		channel := Channel{}

		//fmt.Println(fmt.Sprintf("Processando... i (%d) - (%d)", i, n))
		total := 3
		start := time.Now()
		for i := 1; i <= total; i++ {
			channel.runBufferedQTD(i, done)
			fmt.Println(fmt.Sprintf("i: %d, done: %v", i, <- done))
			fmt.Println(fmt.Sprintf("i: %d, done: %v", i, <- done))
		}

		fmt.Println(fmt.Sprintf("elapse time: %f - qtd channels: %v", time.Now().Sub(start).Seconds(), cap(done)))

	})

	fmt.Println(fmt.Sprintf(">>>>>> %s <<<<<<<", "FIM"))

}

func Test_Channel_Buffered_Sync(t *testing.T) {

	//https://gobyexample.com/channel-synchronization
	t.Run("teste channel", func(t *testing.T) {

		done := make(chan string, 1)
		channel := Channel{}

		//fmt.Println(fmt.Sprintf("Processando... i (%d) - (%d)", i, n))
		total := 10
		start := time.Now()
		for i := 1; i <= total; i++ {
			go channel.runBufferedQTD(i, done)
		}

		for i := 1; i <= total * 2; i++ {
			fmt.Println(fmt.Sprintf("i: %d, done: %v", i, <- done))
		}
		fmt.Println(fmt.Sprintf("elapse time: %f - qtd channels: %v", time.Now().Sub(start).Seconds(), cap(done)))

	})

	fmt.Println(fmt.Sprintf(">>>>>> %s <<<<<<<", "FIM"))

}